#include <iostream>
#include <iomanip>
#include <string>

class Student
{
public:
	Student()
		: mStudentNumber(0)
		, mScore(0)
		, mAverage(0)
	{
	}

	~Student()
	{
		delete[] mScore;
	}

	void SetCount(int StudentNumber)
	{
		mStudentNumber = StudentNumber;
		mScore = new int[mStudentNumber];
		SetNumber(StudentNumber);
	}

	int GetStudentNumber() const
	{
		return mStudentNumber;
	}

	float GetScore(int index) const
	{
		return static_cast<float>(mScore[index]);
	}

	float GetAverage() const
	{
		return mAverage;
	}

private:
	void SetNumber(int StudentNumber)
	{
		for (int i = 0; i < StudentNumber; ++i)
		{
			std::cin >> mScore[i];

			if (!std::cin.fail())
			{
				if (!(0 <= mScore[i] && mScore[i] <= 100))
				{
					--i;
				}
				else
				{
					mAverage += static_cast<float>(mScore[i]);
				}
			}

			else      // 쓰레기 값이 들어오면 버림.
			{
				std::string trash;
				std::cin.clear();
				std::cin >> trash;	// cstring 헤더와 string 헤더 차이점. >>는 string 헤더에서 오버로딩됨.
				continue;
			}
		}
		mAverage /= static_cast<float>(mStudentNumber);
	}

	int mStudentNumber;
	int* mScore;

	float mAverage;
};

int main(void)
{
	int stuNumber = 0;
	std::cin >> stuNumber;
	for (int i = 0; i < stuNumber; ++i)
	{
		int studentNumber = 0;
		std::cin >> studentNumber;
		Student student;
		student.SetCount(studentNumber);

		int stuRatio = 0;
		for (int j = 0; j < student.GetStudentNumber(); ++j)
		{
			if (student.GetAverage() < student.GetScore(j))
			{
				++stuRatio;
			}
		}
		float result = static_cast<float>(stuRatio) / static_cast<float>(studentNumber) * 100;
		std::cout << std::fixed << std::setprecision(3);
		std::cout << result << '%' << std::endl;
	}

	return 0;
}