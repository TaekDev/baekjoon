#include <iostream>

// 2839�� Ǯ��. 
class Sugar
{
public:
	Sugar(int weight);
	void PrintSugar() const;

private:
	void SetThreeFiveKG();

	int mWeight;
	int mThreeKG;
	int mFiveKG;
	bool mbRight;
};

Sugar::Sugar(int weight)
	: mWeight(weight)
	, mThreeKG(0)
	, mFiveKG(0)
	, mbRight(true)
{
	SetThreeFiveKG();
}

void Sugar::SetThreeFiveKG()
{
	while (true)
	{
		if (0 < mWeight && mWeight < 3)
		{
			mbRight = false;
			break;
		}
		else if (mWeight == 0)
		{
			break;
		}
		else if (mWeight % 5 != 0)
		{
			mThreeKG++;
			mWeight -= 3;
		}
		else
		{
			mFiveKG++;
			mWeight -= 5;
		}
	}
}

void Sugar::PrintSugar() const
{
	if (!mbRight)
	{
		std::cout << -1;
	}
	else
	{
		std::cout << mThreeKG + mFiveKG;
	}
}

int main(void)
{
	int weight = 0;
	std::cin >> weight;
	Sugar sugar(weight);
	sugar.PrintSugar();

	return 0;
}